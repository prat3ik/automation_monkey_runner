package tests;

import io.appium.java_client.android.AndroidDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageobjects.AndroidAppPO;
import pageobjects.IOSAppPO;
import utils.CSVUtils;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 25/01/19
 * @project ui.automation
 */
public class AndroidAppTest extends BaseTest {
    @BeforeTest
    @Override
    public void setUpPage() throws MalformedURLException {
        appiumDriver = new AndroidDriver(new URL(APPIUM_SERVER_URL), getDesiredCapabilitiesForAndroid());
    }

    @Test
    public void clickOnButtonOrTypeInToFieldIfFound() {
        AndroidAppPO appPO = new AndroidAppPO(appiumDriver);
        appPO.executeScript();
    }
}
