package pageobjects;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 24/10/18
 * @project ui.automation
 */
public class UserPopUpUI extends Frame implements ActionListener {

    public String inputText;

    public UserPopUpUI(){
        Label l = new Label();
        l.setBounds(25,130,100, 20);
        l.setText("Enter the text:");
        TextField tf = new TextField();
        tf.setBounds(25,150,250,30);
        Button b=new Button("Type into Textfield");
        b.setBounds(75,180,150,30);
        add(l);
        add(tf);
        add(b);//adding button into frame
        setSize(300,300);//frame size 300 width and 300 height
        setLayout(null);
        setVisible(true);//now frame will be visible, by default not visible

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = tf.getText();
                //System.out.println(text);
                setTextForTextView(text);
                dispose();
             //   System.exit(0);
            }
        });
    }

    public void setTextForTextView(String inputText){
        this.inputText = inputText;
    }

    public String getTextForTextView(){
        return inputText;
    }

    public static void main(String[] args) throws InterruptedException {

        int i = ThreadLocalRandom.current().nextInt(0, 3 + 1);
        System.out.println(i);
        System.out.println(ThreadLocalRandom.current().nextInt(0, 3 + 1));
        System.out.println(ThreadLocalRandom.current().nextInt(0, 3 + 1));
        System.out.println(ThreadLocalRandom.current().nextInt(0, 3 + 1));
        System.out.println(ThreadLocalRandom.current().nextInt(0, 3 + 1));
        System.out.println(ThreadLocalRandom.current().nextInt(0, 3 + 1));
        System.out.println(ThreadLocalRandom.current().nextInt(0, 3 + 1));
        System.out.println(ThreadLocalRandom.current().nextInt(0, 3 + 1));
        System.out.println(ThreadLocalRandom.current().nextInt(0, 3 + 1));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }
}
