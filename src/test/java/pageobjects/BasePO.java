/*
 * (C) Copyright 2018 by Pratik Patel (https://github.com/prat3ik/)
 */
package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import utils.PropertyUtils;
import utils.WaitUtils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * Base Page Object Class: This class assign the driver instance and WaitLibrary
 *  @author prat3ik
 */

public abstract class BasePO {
    public final static int IMPLICIT_WAIT = PropertyUtils.getIntegerProperty("implicitWait", 30);
    private static final int KEYBOARD_ANIMATION_DELAY = 1000;
    private static final int XML_REFRESH_DELAY = 1000;
    WaitUtils waitUtils;
    protected final AppiumDriver driver;
    private static List<String> uiWidgetText = new ArrayList<String>();
    private static String inputText = "";
    public final static String CSV_FILE_1_LOCATION = PropertyUtils.getProperty("csv.file1.location", "csvFile1");
    public final static String CSV_FILE_2_LOCATION = PropertyUtils.getProperty("csv.file2.location", "csvFile2");
    public static List<String> inputTextForTextField = new ArrayList<String>();

    /**
     * A base constructor that sets the page's driver
     *
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     *
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    protected BasePO(AppiumDriver driver){
        this.driver = driver;
        initElements();
        loadProperties();
        waitUtils = new WaitUtils();
    }


    private void initElements() {
        PageFactory.initElements(new AppiumFieldDecorator(driver, Duration.ofSeconds(IMPLICIT_WAIT)), this);
    }

    private void loadProperties() {

    }

    public static List<String> getUiWidgetText() {
        return uiWidgetText;
    }

    public static List<String> getFormattedUiWidgetText() {
        List<String> uiWidgetText = getUiWidgetText();
        for (String text : uiWidgetText) {
            if (text.contains(" ")) {
                String newText = text.replaceAll(" ", ",");
                uiWidgetText.set(uiWidgetText.indexOf(text), newText);
            }
        }
        return uiWidgetText;
    }

    public static void addUiWidgetTextToList(String uiWidgetText) {
        getUiWidgetText().add(uiWidgetText);
    }

    public static void clearUiWidgetTextList(){
        getUiWidgetText().clear();
    }

    public static void setInputText(String inputText) {
        BasePO.inputText = inputText;
    }

    public static String getInputText() {
        return inputText;
    }

    public static List<String> getInputTextForTextField() {
        return inputTextForTextField;
    }

    public static void addInputTextForTextField(String inputTextForTextField) {
        getInputTextForTextField().add(inputTextForTextField);
    }
}