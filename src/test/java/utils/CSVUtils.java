package utils;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Year: 2019-20
 *
 * @author Prat3ik on 23/03/19
 * @project ui.automation
 */
public class CSVUtils {

    public static void writeToCSV(String[] inputText, String csvFileLocation) {
        boolean isFileExists = new File(csvFileLocation).exists();

        try {
//            CSVWriter writer = new CSVWriter(
//                    new OutputStreamWriter(new FileOutputStream(csvFileLocation), StandardCharsets.UTF_8),
//                    ',',
//                    CSVWriter.NO_QUOTE_CHARACTER,
//                    CSVWriter.NO_QUOTE_CHARACTER,
//                    CSVWriter.DEFAULT_LINE_END
//            );

            FileWriter writer = new FileWriter(csvFileLocation, true);
            for (int i = 0; i < inputText.length; i++) {
                if (i > 0) writer.append(",");
                writer.append(inputText[i]);
            }
            writer.append("\n");
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}