package tests;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageobjects.IOSAppPO;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 23/10/18
 * @project ui.automation
 */
public class IOSAppTest extends BaseTest {

    @BeforeTest
    @Override
    public void setUpPage() throws MalformedURLException {
        appiumDriver = new IOSDriver(new URL(APPIUM_SERVER_URL), getDesiredCapabilitiesForIOS());
    }

    @Test
    public void clickOnButtonOrTypeInToFieldIfFound() {
        IOSAppPO appPO = new IOSAppPO(appiumDriver);
        appPO.executeScript();
    }

    //TODO: Script will fail when to click on hidden button!
}