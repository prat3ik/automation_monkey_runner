package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSElement;
import org.openqa.selenium.support.FindBy;
import utils.AppiumUtils;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 23/10/18
 * @project ui.automation
 */
public class IOSAppPO extends BasePO {

    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    public IOSAppPO(AppiumDriver driver) {
        super(driver);
    }

    @FindBy(className = "XCUIElementTypeButton")
    IOSElement button;

    private IOSElement getButton() {
        return button;
    }

    private boolean isButtonFound() {
        return AppiumUtils.isElementDisplayed(getButton());
    }

    /**
     * This will get the Random Button from the screen, but use this method once you confirm there are buttons present on screen
     *
     * @return
     */
    private IOSElement getRandomButtonFromScreen() {
        List<IOSElement> buttons = driver.findElementsByClassName("XCUIElementTypeButton");
        int size = buttons.size();
        int randomValue = ThreadLocalRandom.current().nextInt(0, size);
        return buttons.get(randomValue);
    }

    public void tapOnButton() {
        IOSElement randomButtonFromScreen = getRandomButtonFromScreen();
        System.out.println("Tapping on: '" + randomButtonFromScreen.getText() + "' Button");
        randomButtonFromScreen.click();

        // Look whether TextField is appear.
//        if (isTextFieldFound())
//            typeInTextField();
//        else
//            System.out.println("AUTOMATION FINISHED: No TextField Found on Screen OR TextField might be hidden, You can find the screenshot(of current screen) here: screenshots/Success");
    }

    @FindBy(className = "XCUIElementTypeTextField")
    IOSElement textField;

    private IOSElement getTextField() {
        return textField;
    }

    private boolean isTextFieldFound() {
        return AppiumUtils.isElementDisplayed(getTextField());
    }

    /**
     * This will get the Random TextField from the screen, but use this method once you confirm there are TextFields present on screen
     *
     * @return
     */
    private IOSElement getRandomTextFieldFromScreen() {
        List<IOSElement> textFields = driver.findElementsByClassName("XCUIElementTypeTextField");
        int size = textFields.size();
        int randomValue = ThreadLocalRandom.current().nextInt(0, size);
        return textFields.get(randomValue);
    }

    public IOSElement typeInTextField() {
        UserPopUpUI userPopUpUI = new UserPopUpUI();

        // Waiting till Text Value to be entered
        while (userPopUpUI.getTextForTextView() == null) {
            System.out.print("");
        }
        String inputForTextField = userPopUpUI.getTextForTextView();
        System.out.println("Typing: '" + inputForTextField + "' in TextField");
        IOSElement randomTextFieldFromScreen = getRandomTextFieldFromScreen();
        randomTextFieldFromScreen.clear();
        randomTextFieldFromScreen.sendKeys(inputForTextField);
        driver.hideKeyboard();
        return randomTextFieldFromScreen;

        // Look whether Button is appear.
//        if (isButtonFound())
//            tapOnButton();
//        else
//            System.out.println("AUTOMATION FINISHED: No Button Found on Screen OR Button might be hidden, You can find the screenshot(of current screen) here: screenshots/Success");
    }

    /**
     * Textfield			Button			RESULT
     * -----------------------------------------------------------------
     * 0					0				END
     * 0					1				AFTER PRESSING THE BUTTON NEED TO CHECK FOR TEXTFIELD
     * 1					0				ENTER THE TEXT TO TEXTFIELD
     * 1					1				AFTER ENTERING THE TEXT TO TEXTFIELD IT WILL CLICK ON BUTTON
     */
    public void executeScript() {
        boolean isTextFieldFound = isTextFieldFound();
        boolean isButtonFound = isButtonFound();

        ISBUTTON:
        while(true) {
            if ((isTextFieldFound && isButtonFound) == true) {
                typeInTextField();
                tapOnButton();
                isTextFieldFound = isTextFieldFound();
                isButtonFound = isButtonFound();
            } else if ((isTextFieldFound == false) && (isButtonFound == true)) {
                tapOnButton();
                isTextFieldFound = isTextFieldFound();
                isButtonFound = isButtonFound();
            } else if ((isTextFieldFound == true) && (isButtonFound == false)) {
                IOSElement iosElement = typeInTextField();
                int numberOfTextFields = this.driver.findElementsByClassName("android.widget.EditText").size();
                isTextFieldFound = isTextFieldFound();
                isButtonFound = isButtonFound();
                if (numberOfTextFields == 1) {
                    if (!isButtonFound)
                        break ISBUTTON;
                }
            } else {
                System.out.println("The current screen of App doesn't have any TextField or Button. You can find the screenshot(of current screen) here: screenshots/Success");
                break;
            }
        }
    }
}