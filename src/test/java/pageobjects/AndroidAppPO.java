package pageobjects;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.support.FindBy;
import utils.AppiumUtils;
import utils.CSVUtils;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Year: 2018-19
 *
 * @author Prat3ik on 23/10/18
 * @project ui.automation
 */
public class AndroidAppPO extends BasePO {

    /**
     * A base constructor that sets the page's driver
     * <p>
     * The page structure is being used within this test in order to separate the
     * page actions from the tests.
     * <p>
     * Please use the AppiumFieldDecorator class within the page factory. This way annotations
     * like @AndroidFindBy within the page objects.
     *
     * @param driver the appium driver created in the beforesuite method.
     */
    public AndroidAppPO(AppiumDriver driver) {
        super(driver);
    }

    @FindBy(className = "android.widget.Button")
    AndroidElement button;

    private AndroidElement getButton() {
        return button;
    }

    private boolean isButtonFound() {
        return AppiumUtils.isElementDisplayed(getButton());
    }

    /**
     * This will get the Random Button from the screen, but use this method once you confirm there are buttons present on screen
     *
     * @return
     */
    private AndroidElement getRandomButtonFromScreen() {
        List<AndroidElement> buttons = driver.findElementsByClassName("android.widget.Button");
        int size = buttons.size();
        int randomValue = ThreadLocalRandom.current().nextInt(0, size);
        return buttons.get(randomValue);
    }

    public void tapOnButton() {
        AndroidElement randomButtonFromScreen = getRandomButtonFromScreen();
        System.out.println("Tapping on: '" + randomButtonFromScreen.getText() + "' Button");
        addUiWidgetTextToList(randomButtonFromScreen.getText());
        System.out.println("Button: '" + randomButtonFromScreen.getText() + "' added to the list.");
        randomButtonFromScreen.click();
    }

    @FindBy(className = "android.widget.EditText")
    AndroidElement textField;

    private AndroidElement getTextField() {
        return textField;
    }

    private boolean isTextFieldFound() {
        return AppiumUtils.isElementDisplayed(getTextField());
    }

    /**
     * This will get the Random TextField from the screen, but use this method once you confirm there are TextFields present on screen
     *
     * @return
     */
    private AndroidElement getRandomTextFieldFromScreen() {
        List<AndroidElement> textFields = driver.findElementsByClassName("android.widget.EditText");
        int size = textFields.size();
        int randomValue = ThreadLocalRandom.current().nextInt(0, size);
        return textFields.get(randomValue);
    }

    public void typeInTextField() {
        String inputForTextField;
        List<AndroidElement> textFieldElements = this.driver.findElementsByClassName("android.widget.EditText");
        if (textFieldElements.size() == 1) {
            inputForTextField = getInputTextFromUser();
            AndroidElement androidElement = textFieldElements.get(0);
            System.out.println("Typing: '" + inputForTextField + "' in TextField");
            addUiWidgetTextToList(androidElement.getText());
            typeInParticularTextField(inputForTextField, androidElement);
        } else{
            System.out.println("Entered in Else...");
            AndroidElement randomTextFieldFromScreen = getRandomTextFieldFromScreen();
            String textOfTextField = randomTextFieldFromScreen.getText();
            System.out.println("TF value:"+textOfTextField);
            System.out.println("TF Array input values:"+getInputTextForTextField());
            if(getInputTextForTextField().contains(textOfTextField)){
                //SKIP
                System.out.println("Skipeed");
            }else {
                addUiWidgetTextToList(textOfTextField);
                inputForTextField = getInputTextFromUser();
                addInputTextForTextField(inputForTextField);
                typeInParticularTextField(inputForTextField, randomTextFieldFromScreen);
            }
        }
//
//        AndroidElement randomTextFieldFromScreen = getRandomTextFieldFromScreen();
//        addUiWidgetTextToList(randomTextFieldFromScreen.getText());
//        System.out.println("TextView: '" + randomTextFieldFromScreen.getText() + "' added to the list.");
//        typeInParticularTextField(inputForTextField, randomTextFieldFromScreen);

//        System.out.println("All values from array:" + getUiWidgetText());
        return;
    }

    private void typeInTextFieldSequentially() {
        List<AndroidElement> textFieldElements = this.driver.findElementsByClassName("android.widget.EditText");
        for (AndroidElement el : textFieldElements) {
            String inputTextFromUser = getInputTextFromUser();
            addUiWidgetTextToList(el.getText());
            typeInParticularTextField(inputTextFromUser, el);
        }
    }

    private String getInputTextFromUser() {
        UserPopUpUI userPopUpUI = new UserPopUpUI();

        // Waiting till Text Value to be entered
        while (userPopUpUI.getTextForTextView() == null) {
            System.out.print("");
        }
        return userPopUpUI.getTextForTextView();
    }

    private void typeInParticularTextField(String inputForTextField, AndroidElement textFieldElement) {
        setInputText(inputForTextField);
        CSVUtils.writeToCSV(getFormattedUiWidgetText().toArray(new String[getUiWidgetText().size()]) ,CSV_FILE_1_LOCATION);
        CSVUtils.writeToCSV(new String[]{inputForTextField},CSV_FILE_2_LOCATION);
        clearUiWidgetTextList();
        textFieldElement.clear();
        textFieldElement.sendKeys(inputForTextField);
    }

    /**
     * Textfield			Button			RESULT
     * -----------------------------------------------------------------
     * 0					0				END
     * 0					1				AFTER PRESSING THE BUTTON NEED TO CHECK FOR TEXTFIELD
     * 1					0				ENTER THE TEXT TO TEXTFIELD
     * 1					1				AFTER ENTERING THE TEXT TO TEXTFIELD IT WILL CLICK ON BUTTON
     */
    public void executeScript() {
        boolean isTextFieldFound = isTextFieldFound();
        boolean isButtonFound = isButtonFound();

        LOOP:
        while (true) {
            if ((isTextFieldFound && isButtonFound) == true) {
                System.out.println("TF & Button both found!");
                typeInTextField();
                typeInTextFieldSequentially();
                isTextFieldFound = isTextFieldFound();
                isButtonFound = isButtonFound();
            } else if ((isTextFieldFound == false) && (isButtonFound == true)) {
                tapOnButton();
                isTextFieldFound = isTextFieldFound();
                isButtonFound = isButtonFound();
            } else if ((isTextFieldFound == true) && (isButtonFound == false)) {
                System.out.println("TF Only found!");
                typeInTextFieldSequentially();
                isButtonFound = isButtonFound();
                if (isButtonFound)
                    isTextFieldFound = isTextFieldFound();
                else break LOOP;
            } else {
                System.out.println("AUTOMATION FINISHED:::The current screen of App doesn't have any TextField or Button. You can find the screenshot(of current screen) here: screenshots/Success");
                break;
            }
        }
    }
}